package com.recko.assignment.entity;

import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Harman Singh on 31 May, 2018; 14:25 IST
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Person")
public class Person {

  @ApiModelProperty(hidden = true)
  @Id
  @GeneratedValue
  private UUID id;

  private String familyId;

  private String universeId;

  private Long power;

}
