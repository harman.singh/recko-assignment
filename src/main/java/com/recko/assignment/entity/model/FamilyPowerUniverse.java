package com.recko.assignment.entity.model;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Harman Singh on 31 May, 2018; 14:41 IST
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FamilyPowerUniverse {

  private String familyId;
  private Map<String, Long> powerInUnverse;
}
