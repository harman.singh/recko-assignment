package com.recko.assignment.entity.model;

import java.util.Map;
import lombok.Data;

/**
 * Number of people changed in family in each universe
 */
@Data
public class FamilyUpdateUniverse {
  private String familyId;
  private Map<String, Integer> peopleAdded;
}
