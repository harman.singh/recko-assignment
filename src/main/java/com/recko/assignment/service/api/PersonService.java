package com.recko.assignment.service.api;

import com.recko.assignment.entity.Person;
import com.recko.assignment.entity.model.FamilyPowerUniverse;
import java.util.List;

/**
 * Created by Harman Singh on 31 May, 2018; 14:29 IST
 */
public interface PersonService {

  /**
   * Find all families in a universe.
   */
  List<String> findFamiliesInUnverse(String universeId);

  /**
   * Check if families with same identifiers have same power in all universes
   */
  List<FamilyPowerUniverse> findUnbalancedFamilies();

  /**
   * Find unbalanced families and balance them
   */
  List<Person> balanceFamilies();

  /**
   * Add a person
   */
  Person save(Person person);
}
