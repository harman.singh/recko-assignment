package com.recko.assignment.dao;

import com.recko.assignment.entity.Person;
import com.recko.assignment.entity.model.FamilyGrouped;
import com.recko.assignment.entity.model.FamilyPowerUniverse;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Harman Singh on 31 May, 2018; 14:29 IST
 */
public interface PersonRepository extends JpaRepository<Person, String> {

  @Query("Select P.familyId from Person P where P.universeId=:universeId")
  List<String> findAllFamilies(@Param("universeId") String universeId);

  @Query("Select new com.recko.assignment.entity.model.FamilyGrouped(P.familyId, P.universeId, SUM(P.power))"
      + " from Person P group by P.familyId, P.universeId")
  List<FamilyGrouped> findFamilyPowers();
}
