## Instructions

- `git clone https://gitlab.com/elixdragon/recko-assignment`
- Set db config in `application.properties` file
- Build: `mvn clean install` to generate .jar
- Deploy: `java -jar target/recko.jar`
- Browse: `localhost:4444/swagger-ui.html`

